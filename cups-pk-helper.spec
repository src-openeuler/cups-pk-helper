Name:           cups-pk-helper
Version:        0.2.7
Release:        1
Summary:        Cups config helper
License:        GPLv2+

URL:            http://www.freedesktop.org/wiki/Software/cups-pk-helper/
Source0:        http://www.freedesktop.org/software/cups-pk-helper/releases/cups-pk-helper-%{version}.tar.xz

BuildRequires:  libtool >= 1.4.3 cups-devel >= 1.2 glib2-devel >= 2.29.8 gtk2-devel >= 2.12.0
BuildRequires:  dbus-glib-devel >= 0.74 polkit-devel >= 0.97 polkit-gnome >= 0.97
BuildRequires:  intltool >= 0.40.6 gettext-devel >= 0.17 gnome-common >= 2.26
BuildRequires:  meson

Requires:       cups-libs >= 1.2 dbus-glib >= 0.74 dbus >= 1.2 glib2 >= 2.29.8

%description
This package provides a helper to configure cups.

%prep
%autosetup -p1

%build
%meson
%meson_build

%install
%meson_install

%find_lang %{name}

%files -f %{name}.lang
%doc AUTHORS COPYING NEWS
%{_datadir}/dbus-1/system.d/org.opensuse.CupsPkHelper.Mechanism.conf
%{_libexecdir}/cups-pk-helper-mechanism
%{_datadir}/dbus-1/system-services/org.opensuse.CupsPkHelper.Mechanism.service
%{_datadir}/polkit-1/actions/org.opensuse.cupspkhelper.mechanism.policy

%changelog
* Fri Oct 20 2023 li weigang - <weigangli99@gmail.com> - 0.2.7-1
- Type: update
- ID: NA
- SUG: NA
- DESC: update to version 0.2.7

* Thu Jan 9 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.2.6-8
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete patches

* Sat Sep 7 2019 guan yanjie <guanyanjie@huawei.com> - 0.2.6-7
- Package init for openEuler
